<?php

namespace mock\annotation\illustrate;

use Doctrine\Common\Annotations\Annotation;

/**
 * 路由分组
 * @package mock\annotation\illustrate
 * @Annotation
 * @Target({"CLASS","METHOD"})
 */
final class Group extends Rule
{

}
