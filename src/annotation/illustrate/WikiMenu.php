<?php

namespace mock\annotation\illustrate;

use Doctrine\Common\Annotations\Annotation;

/**
 * 文档
 *
 * @package mock\annotation\illustrate
 * @Annotation
 * @Target({"METHOD","CLASS"})
 */
class WikiMenu extends Annotation
{}
