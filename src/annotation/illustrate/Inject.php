<?php

namespace mock\annotation\illustrate;

use Doctrine\Common\Annotations\Annotation;

/**
 * 属性自动依赖注入
 * @package mock\annotation\illustrate
 * @Annotation
 * @Target({"PROPERTY"})
 */
final class Inject extends Annotation
{

}
