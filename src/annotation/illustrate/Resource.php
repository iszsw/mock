<?php

namespace mock\annotation\illustrate;

use Doctrine\Common\Annotations\Annotation;
use Doctrine\Common\Annotations\Annotation\Target;

/**
 * 注册资源路由
 * @package mock\annotation\illustrate
 * @Annotation
 * @Target({"CLASS"})
 */
final class Resource extends Rule
{

}
