<?php

namespace mock\annotation\illustrate;

use Doctrine\Common\Annotations\Annotation;

/**
 * Class Validate
 * @package mock\annotation\illustrate
 * @Annotation
 * @Annotation\Target({"METHOD"})
 */
final class Validate extends Annotation
{
    /**
     * @var string
     */
    public $scene;

    /**
     * @var array
     */
    public $message = [];

    /**
     * @var bool
     */
    public $batch = true;
}
